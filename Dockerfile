FROM jruby:1.7.26

ADD ./oadr /usr/local/oadr

EXPOSE 8300 8383 8585

WORKDIR /usr/local/oadr/

ENV HTTP_PROXY 161.27.206.250:8080
ENV HTTPS_PROXY 161.27.206.250:8080
RUN export http_proxy=$HTTP_PROXY
RUN export https_proxy=$HTTPS_PROXY

RUN jruby -J-Xmx1024m -S bundle install

CMD ["rake", "assets:precompile"]

CMD ["bundle", "exec", "puma", "-C", "config/puma.rb"]
