=====================================================
Dockerized VTN server implementation - v1.0
=====================================================


.:: CONTENTS ::.

This readme allows the installation of the VTN server released through Docker. The VTN server consists of two components:
1) The EPRI VTN server ('vtn')
2) The database supporting the VTN server ('vtn-db')


.:: SETUP ::.

BUILD AND RUN DOCKER IMAGES

Execute in the folder 'docker_vtnserver' the command for setting up Docker compose:
	docker-compose up

DATABASE INITIALISATION

Once started, access to the 'vtn' container and execute the commands for DB initialisation:
	rake db:setup RAILS_ENV=production
	rake db:seed
Now, exit the 'vtn' bash and restart Docker compose.


.:: ACCESS TO THE VTN SERVER GUI ::.

The EPRI VTN server implementation provides a GUI that is available at:
	localhost:8383
Credentials:
	user: admin
	password: testing


.:: CONFIGURATION ::.

The key parameters to configure for setting-up the VTN are the following:
- VTN server: using the VTN GUI, create a new VEN by defining:
	1) name of the VEN
	2) VEN ID (automatically generated)
	3) market context (to be created before its selection in the VEN options)
	4) schema version (2.0b)


.:: CHANGELOG ::.

v1.0 Tested on the development machine


.:: CONTACTS ::.

alessio.roppolo@eng.it
