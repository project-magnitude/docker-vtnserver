CREATE USER epri;

CREATE DATABASE epri_oadr;
CREATE DATABASE epri_oadr_dev;
CREATE DATABASE epri_oadr_test;
GRANT ALL PRIVILEGES ON DATABASE epri_oadr TO epri_oadr;
GRANT ALL PRIVILEGES ON DATABASE epri_oadr TO epri_oadr_dev;
GRANT ALL PRIVILEGES ON DATABASE epri_oadr TO epri_oadr_test;

ALTER USER epri CREATEDB;
